use axum::{
    extract::Query, http::HeaderMap, http::StatusCode, response::Json, routing::get, Router,
};
use reqwest::Method;
use serde_json::{json, Value};
use std::{collections::HashMap, error::Error, sync::OnceLock};
use tracing_opentelemetry::OpenTelemetrySpanExt as _;

mod otel;

static SERVER_PORT: OnceLock<u32> = OnceLock::new();

#[tracing::instrument(skip_all)]
fn success() -> Result<Json<Value>, StatusCode> {
    tracing::info!("here is THE answer!");
    Ok(Json(json!({ "answer": 42 })))
}

#[tracing::instrument(skip_all)]
fn error() -> Result<Json<Value>, StatusCode> {
    tracing::error!("almost there but wrong port!");
    Err(StatusCode::INTERNAL_SERVER_ERROR)
}

#[autometrics::autometrics]
#[tracing::instrument(skip_all)]
async fn forward_request(addr: &str, forward_port: u32) -> Result<reqwest::Response, StatusCode> {
    let mut request = reqwest::Request::new(Method::GET, addr.parse().unwrap());
    opentelemetry::global::get_text_map_propagator(|propagator| {
        propagator.inject_context(
            &tracing::Span::current().context(),
            &mut opentelemetry_http::HeaderInjector(request.headers_mut()),
        );
    });

    reqwest::Client::new().execute(request).await.map_err(|_e| {
        tracing::error!(
            host = "localhost",
            port = forward_port,
            "service not reachable"
        );
        StatusCode::INTERNAL_SERVER_ERROR
    })
}

#[autometrics::autometrics]
#[tracing::instrument(skip_all)]
async fn handle_forward(forward: &str) -> Result<Value, StatusCode> {
    let (forward_port, forward_rest) =
        if let Some((forward_port, forward_rest)) = forward.split_once('|') {
            (forward_port, forward_rest)
        } else {
            (forward, "")
        };
    let Ok(forward_port) = forward_port.parse::<u32>() else {
        tracing::error!("'forward' must be an integer");
        return Err(StatusCode::INTERNAL_SERVER_ERROR);
    };
    let mut addr = format!("http://0.0.0.0:{forward_port}");
    if !forward_rest.is_empty() {
        addr += &format!("?forward={forward_rest}");
    }
    let response = forward_request(&addr, forward_port).await?;
    let response = response
        .json::<Value>()
        .await
        .map_err(|_e| StatusCode::INTERNAL_SERVER_ERROR)?;
    Ok(response)
}

#[autometrics::autometrics]
#[tracing::instrument(skip_all)]
async fn root(
    Query(params): Query<HashMap<String, String>>,
    headers: HeaderMap,
) -> Result<Json<Value>, StatusCode> {
    let parent_cx = opentelemetry::global::get_text_map_propagator(|propagator| {
        propagator.extract(&opentelemetry_http::HeaderExtractor(&headers))
    });
    tracing::Span::current().set_parent(parent_cx);
    match SERVER_PORT.get() {
        Some(4242) => success(),
        Some(4243) => error(),
        None => unreachable!("this service is started, and therefore has a port"),
        Some(_port) => {
            let Some(forward) = params.get("forward") else {
                tracing::error!("no 'forward' query param");
                return Err(StatusCode::INTERNAL_SERVER_ERROR);
            };
            let mut responses = vec![];
            for forward in forward.split(':') {
                responses.push(handle_forward(forward).await?);
            }
            tracing::info!("gathered all the answers from services");
            Ok(Json(Value::from(responses)))
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    let port: u32 = std::env::var("SERVER_PORT")
        .expect("'SERVER_PORT' needs to be defined")
        .parse()
        .expect("'SERVER_PORT' needs to be an integer");
    SERVER_PORT.get_or_init(|| port);
    #[allow(unused_variables)]
    let (_trace_guard, metrics_guard) = otel::init()?;
    let app = Router::new().route("/", get(root));
    #[cfg(feature = "prometheus-metrics")]
    let app = app.route(
        "/metrics",
        get(|| async { autometrics::prometheus_exporter::encode_http_response() }),
    );

    // run it with hyper on localhost:3000
    tracing::info!(host = "localhost", port, "Starting server");
    axum::Server::bind(&format!("0.0.0.0:{port}").parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();

    // Shutting down
    opentelemetry::global::shutdown_tracer_provider();
    #[cfg(not(feature = "prometheus-metrics"))]
    metrics_guard.force_flush(&tracing::Span::current().context())?;
    #[cfg(not(feature = "prometheus-metrics"))]
    metrics_guard.shutdown()?;
    Ok(())
}
