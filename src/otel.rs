use opentelemetry::sdk::propagation::{
    BaggagePropagator, TextMapCompositePropagator, TraceContextPropagator,
};
use std::error::Error;
use tracing_subscriber::{layer::SubscriberExt as _, util::SubscriberInitExt as _};

fn init_tracer() {
    let propagators = TextMapCompositePropagator::new(vec![
        Box::new(TraceContextPropagator::new()),
        Box::new(BaggagePropagator::new()),
    ]);
    opentelemetry::global::set_text_map_propagator(propagators);
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::builder()
                .with_default_directive(tracing_subscriber::filter::LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with(tracing_subscriber::fmt::layer().pretty())
        .with(
            tracing_opentelemetry::layer().with_tracer(
                opentelemetry_otlp::new_pipeline()
                    .tracing()
                    .with_exporter(opentelemetry_otlp::new_exporter().tonic())
                    .install_batch(opentelemetry::runtime::Tokio)
                    .unwrap(),
            ),
        )
        .init();
}

#[cfg(not(feature = "prometheus-metrics"))]
type MetricGuard = opentelemetry::sdk::metrics::MeterProvider;
#[cfg(not(feature = "prometheus-metrics"))]
fn init_metrics() -> Result<MetricGuard, Box<dyn Error + Send + Sync + 'static>> {
    use opentelemetry_otlp::{ExportConfig, Protocol, WithExportConfig as _};
    use std::time::Duration;

    let export_config = ExportConfig {
        endpoint: "http://localhost:4317".to_string(),
        timeout: Duration::from_secs(3),
        protocol: Protocol::Grpc,
    };
    opentelemetry_otlp::new_pipeline()
        .metrics(opentelemetry::runtime::Tokio)
        .with_exporter(
            opentelemetry_otlp::new_exporter()
                .tonic()
                .with_export_config(export_config),
        )
        .with_period(Duration::from_secs(1))
        .with_timeout(Duration::from_secs(10))
        .build()
        .map_err(Into::into)
}
#[cfg(feature = "prometheus-metrics")]
type MetricGuard = ();
#[cfg(feature = "prometheus-metrics")]
fn init_metrics() -> Result<MetricGuard, Box<dyn Error + Send + Sync + 'static>> {
    Ok(autometrics::prometheus_exporter::init())
}

#[must_use = "The guards must live until the end of the program"]
pub fn init() -> Result<((), MetricGuard), Box<dyn Error + Send + Sync + 'static>> {
    Ok((init_tracer(), init_metrics()?))
}
