set dotenv-load

docker-network-otel:
	-@docker network create otel

otel-collector: docker-network-otel
	-@docker run \
		--rm \
		--detach \
		--name="otel-collector" \
		--network="otel" \
		--hostname="otel-collector" \
		--publish 4317:4317 \
		--publish 4318:4318 \
		--env-file .env \
		--volume "${PWD}/otel-config.yaml:/etc/otelcol-contrib/config.yaml" \
		otel/opentelemetry-collector-contrib:0.85.0

jaeger: docker-network-otel
	-@docker run \
		--rm \
		--detach \
		--name="jaeger" \
		--network="otel" \
		--hostname="jaeger" \
		--publish 16686:16686 \
		jaegertracing/all-in-one:1.49

grafana-volume:
	-@docker volume create grafana-storage
	
grafana: docker-network-otel grafana-volume
	-@docker run \
		--rm \
		--detach \
		--name="grafana" \
		--network="host" \
		--hostname="grafana" \
		--publish 3000:3000 \
		--volume grafana-storage:/var/lib/grafana \
		grafana/grafana-enterprise

prometheus:
	-@docker run \
		--rm \
		--detach \
		--name="prometheus" \
		--network="host" \
		--hostname="prometheus" \
		--publish 9090:9090 \
		--volume "${PWD}/prometheus.yaml:/opt/bitnami/prometheus/prometheus.yml" \
		bitnami/prometheus:latest \
		--enable-feature=exemplar-storage

serve port: otel-collector jaeger
	SERVER_PORT={{port}} OTEL_SERVICE_NAME=serve-{{port}} \
		cargo run --release

serve-prom port: otel-collector jaeger grafana prometheus
	SERVER_PORT={{port}} OTEL_SERVICE_NAME=serve-{{port}} \
		cargo run --release --no-default-features --features prometheus-metrics
